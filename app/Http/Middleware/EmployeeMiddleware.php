<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class EmployeeMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && ((Auth::user()->roles->name) == 'employee' || (Auth::user()->roles->name) == 'owner') ) {
            return $next($request);
        }

        abort(404);
    }

}
