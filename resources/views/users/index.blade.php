@extends('layouts.app')

@section('content')
<div class="container">
    <div class="box-body">
      <div class="form-group">
        <a href="{{route('users.create')}}" class="btn btn-success">Добавить</a>
      </div>
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>ID</th>
          <th>Имя</th>
          <th>E-mail</th>
          <th>Роль</th>
          <th>Действия</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
         <tr>
           <td>{{$user->id}}</td>
           <td>{{$user->name}}</td>
           <td>{{$user->email}}</td>
           <td>{{$user->roles->name}}</td>
           <td><a href="{{route('users.edit', $user->id)}}">edit</a>
           {{Form::open(['route'=>['users.destroy', $user->id], 'method'=>'delete'])}}
           <button onclick="return confirm('are you sure?')" type="submit" class="delete">
            delete
           </button>

            {{Form::close()}}
           </td>
         </tr>
        @endforeach

        </tfoot>
      </table>
    </div>
</div>
@endsection