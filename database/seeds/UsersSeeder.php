<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
          'name'     => 'owner',
          'email'    => 'owner@test.com',
          'password' => bcrypt('123456'),
          'role_id' => 1,
        ]);
    }

}
